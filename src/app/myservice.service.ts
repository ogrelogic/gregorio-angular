import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
@Injectable({
  providedIn: 'root'
})


export class MyserviceService {

    urldashboard = AppComponent.urldashboard; 


    /*socila links api*/ 
    url='http://50.18.189.215:1337/social-media-links';
    constructor(private http:HttpClient) { }
    social()
    {
      return this.http.get(this.url);
    }
    
    /*socila links api end*/ 
    url2='http://50.18.189.215:1337/social-media-links';
    homemedisliders(){
      return this.http.get(this.url2)
    }

    /*home page Welcome To The Gregorio VII Luxury Suites*/
    url3 ='http://50.18.189.215:1337/home-page-sections';
    user(){
      return this.http.get(this.url3);
    }

    /*home banner */
    /*url4='http://localhost:1337/banners';*/
    url4='http://50.18.189.215:1337/home-banners';

    users()
    {
      return this.http.get(this.url4);
    }

    /*home banner */
    url5='http://50.18.189.215:1337/home-media-sliders';
    homegallery()
    {
      return this.http.get(this.url5);
    }
    

    /*suites page url */
    url6='http://localhost:1337/suitespages';
    Suitespages()
    {
      return this.http.get(this.url6);
    }

    /*Direction page*/
    url7='http://localhost:1337/direction-pages';
    directionpages()
    {
      return this.http.get(this.url7);
    }


    url8='http://localhost:1337/special-offers';
    specialoffers()
    {
      return this.http.get(this.url8);
    }

    /*service page apistrapi*/
    url9='http://localhost:1337/services';
    services()
    {
      return this.http.get(this.url9);
    }

    /*post page*/
    url10='http://localhost:1337/blogpages';
    blogpages()
    {
      return this.http.get(this.url10);
    }

    /*near by api*/
    url11='http://localhost:1337/near-bies';
    nearbypage()
    {
      return this.http.get(this.url11);
    }


    /*gallery page data*/
    url12='http://localhost:1337/gallerypages';
    gallerypage()
    {
      return this.http.get(this.url12);
    }

    url13='http://50.18.189.215:1337/logo-footer-address-copyright-sections';
    headerfoorer()
    {
      return this.http.get(this.url13);
    }


    url14='http://localhost:1337/romantic-weekend-packages';
    romanticweekend()
    {
      return this.http.get(this.url14);
    }


    url15='http://localhost:1337/romantic-holiday-packages';
    romanticholiday()
    {
      return this.http.get(this.url15);
    }
    
    url16='http://localhost:1337/honeymoon-suites';
    honeymoonsuites()
    {
      return this.http.get(this.url16);
    }

    url17='http://localhost:1337/superior-suites';
    superiorsuites()
    {
      return this.http.get(this.url17);
    }

    url18="http://50.116.6.254/boxsiteantigua/post-all.php?token=YWJj";
    
    
    create(postData:any)
    {
      return this.http.post(this.url18,postData);
    }

  }


