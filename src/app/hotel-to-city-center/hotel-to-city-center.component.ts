import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-hotel-to-city-center',
  templateUrl: './hotel-to-city-center.component.html',
  styleUrls: ['./hotel-to-city-center.component.css']
})
export class HotelToCityCenterComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.directionpages().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].HOTELTOCITYCENTER;
      console.warn(this.directionpagedata1);
    });
  }

  ngOnInit(): void {
  }

}
