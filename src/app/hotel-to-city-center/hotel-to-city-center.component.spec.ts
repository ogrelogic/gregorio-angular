import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelToCityCenterComponent } from './hotel-to-city-center.component';

describe('HotelToCityCenterComponent', () => {
  let component: HotelToCityCenterComponent;
  let fixture: ComponentFixture<HotelToCityCenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HotelToCityCenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelToCityCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
