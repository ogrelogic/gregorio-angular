import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './header/logo/logo.component';
import { NavigationsComponent } from './header/navigations/navigations.component';
import { SearchComponent } from './header/search/search.component';
import { HomeComponent } from './home/home.component';
import { SlidesComponent } from './home/component/slides/slides.component';
import { Home2Component } from './home/component/home-gallery/home2.component';
import { FootermainComponent } from './footer/footermain/footermain.component';
import { Home3Component } from './home/component/home3/home3.component';
import { TextpartComponent } from './home/component/home4/textpart/textpart.component';
import { ImgpartComponent } from './home/component/home4/imgpart/imgpart.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersbannersComponent } from './home/component/slides/usersbanners/usersbanners.component';
import { SocialLinksComponent } from './home/component/social-media-links/social-links/social-links.component';
import { SocialLinksUrlComponent } from './home/component/social-media-links/social-links-url/social-links-url.component';
import { Home3apiurlComponent } from './home/component/home3/home3apiurl/home3apiurl.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactFormComponent } from './contact-form/contact-form.component';  
import { ContactService } from './contact.service';
import { SuitsPageComponent } from './suits-page/suits-page.component';
import { BannerComponent } from './suits-page/banner/banner.component';
import { OursuitesComponent } from './suits-page/oursuites/oursuites.component';
import { DirectionPagesComponent } from './direction-pages/direction-pages.component';
import { ComponentComponent } from './direction-pages/component/component.component';
import { ContentComponent } from './direction-pages/component/content/content.component';
import { FromCiampinoAirportComponent } from './from-ciampino-airport/from-ciampino-airport.component';
import { FromTerminiTrainStationComponent } from './from-termini-train-station/from-termini-train-station.component';
import { HotelToCityCenterComponent } from './hotel-to-city-center/hotel-to-city-center.component';
import { SpecialOffersComponent } from './special-offers/special-offers.component';
import { ServicespageComponent } from './servicespage/servicespage.component';
import { BlogpageComponent } from './blogpage/blogpage.component';
import { ServicescomponentComponent } from './servicespage/servicescomponent/servicescomponent.component';
import { NearByPageComponent } from './near-by-page/near-by-page.component';
import { GallerypagesComponent } from './gallerypages/gallerypages.component';
import { NewsComponent } from './news/news.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CarouselholderComponent } from './carouselholder/carouselholder.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SinglePostComponent } from './single-post/single-post.component';
import { RomanticHolidayPackageComponent } from './romantic-holiday-package/romantic-holiday-package.component';
import { RomanticWeekendPackagesComponent } from './romantic-weekend-packages/romantic-weekend-packages.component';
import { HoneymoonSuiteComponent } from './honeymoon-suite/honeymoon-suite.component';
import { SuperiorSuiteComponent } from './superior-suite/superior-suite.component';
import { FormsModule } from '@angular/forms';
import { BookingComponent } from './booking/booking.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LogoComponent,
    NavigationsComponent,
    SearchComponent,
    HomeComponent, 
    SlidesComponent, 
    Home2Component, 
    FootermainComponent, 
    Home3Component, 
    TextpartComponent, 
    ImgpartComponent, UsersbannersComponent, SocialLinksComponent, SocialLinksUrlComponent, Home3apiurlComponent, ContactFormComponent, SuitsPageComponent, BannerComponent, OursuitesComponent, DirectionPagesComponent, ComponentComponent, ContentComponent, FromCiampinoAirportComponent, FromTerminiTrainStationComponent, HotelToCityCenterComponent, SpecialOffersComponent, ServicespageComponent, BlogpageComponent, ServicescomponentComponent, NearByPageComponent, GallerypagesComponent, NewsComponent, CarouselholderComponent, SinglePostComponent, RomanticHolidayPackageComponent, RomanticWeekendPackagesComponent, HoneymoonSuiteComponent, SuperiorSuiteComponent, BookingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    ReactiveFormsModule,
    CarouselModule,
    BrowserAnimationsModule, 
    FormsModule
  ],
  providers: [
    ContactService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
