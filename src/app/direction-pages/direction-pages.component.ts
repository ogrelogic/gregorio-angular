import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-direction-pages',
  templateUrl: './direction-pages.component.html',
  styleUrls: ['./direction-pages.component.css']
})
export class DirectionPagesComponent implements OnInit {

  
  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.directionpages().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].FromFiumicinoAirport;
      console.warn(this.directionpagedata1);
    });
  }

  ngOnInit(): void {
  }

}
