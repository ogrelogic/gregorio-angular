import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MyserviceService } from 'src/app/myservice.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.directionpages().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].FromFiumicinoAirport;
      console.warn(this.directionpagedata1);
    });
  }

  ngOnInit(): void {
  }

}
