import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectionPagesComponent } from './direction-pages.component';

describe('DirectionPagesComponent', () => {
  let component: DirectionPagesComponent;
  let fixture: ComponentFixture<DirectionPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectionPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectionPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
