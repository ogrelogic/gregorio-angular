import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-honeymoon-suite',
  templateUrl: './honeymoon-suite.component.html',
  styleUrls: ['./honeymoon-suite.component.css']
})
export class HoneymoonSuiteComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  gallerydata:any;
  honeymoonsuitetitle:any;

  constructor(private userData1:MyserviceService) { 
    this.userData1.honeymoonsuites().subscribe(( data) =>{
      this.directionpages=data;
      this.honeymoonsuitetitle= this.directionpages[0].honeymoonsuite;
      this.directionpagedata1= this.directionpages[0].honeymoonsuite[0].image;
      this.gallerydata= this.directionpages[0].honeymoonsuite[0].slider;
      console.warn(this.gallerydata);
    });
  }

  ngOnInit(): void {
  }

}
