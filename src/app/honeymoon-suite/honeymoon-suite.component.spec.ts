import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HoneymoonSuiteComponent } from './honeymoon-suite.component';

describe('HoneymoonSuiteComponent', () => {
  let component: HoneymoonSuiteComponent;
  let fixture: ComponentFixture<HoneymoonSuiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HoneymoonSuiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoneymoonSuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
