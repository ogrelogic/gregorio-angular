import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blogpage',
  templateUrl: './blogpage.component.html',
  styleUrls: ['./blogpage.component.css']
})
export class BlogpageComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;
  blogpages:any;
  blogpagedata1:any;
  blogpagedata2:any;
  constructor(private userData1:MyserviceService, 
              private router: Router,
              private godata: ActivatedRoute 
              )
  
  { 
    this.userData1.blogpages().subscribe(( data) =>{
      this.blogpages=data;
      this.blogpagedata1= this.blogpages[0].Blogpage;
      this.blogpagedata2= this.blogpages[0].Blogpage.id;
      console.warn(this.blogpagedata2);
    });

  }
  onClick(dataId: number){
    //console.log(this.router.navigate(['single-post/', dataId]));
  }

  ngOnInit(): void {
    console.log(this.godata.snapshot.params);
  }

}
