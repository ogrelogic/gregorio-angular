import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperiorSuiteComponent } from './superior-suite.component';

describe('SuperiorSuiteComponent', () => {
  let component: SuperiorSuiteComponent;
  let fixture: ComponentFixture<SuperiorSuiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperiorSuiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperiorSuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
