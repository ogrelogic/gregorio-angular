import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-superior-suite',
  templateUrl: './superior-suite.component.html',
  styleUrls: ['./superior-suite.component.css']
})
export class SuperiorSuiteComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  gallerydata:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.superiorsuites().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].superiorsuitecomponentds;
      this.gallerydata= this.directionpages[0].superiorsuitecomponentds[0].slider;
      console.warn(this.gallerydata);
    });
  }

  ngOnInit(): void {
  }

}
