import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-carouselholder',
  templateUrl: './carouselholder.component.html',
  styleUrls: ['./carouselholder.component.css']
})
export class CarouselholderComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    items: 1,
    navSpeed: 700,
    navText: ['>', '<'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 1
      }
    },
    nav: true
  }

  //constructor() { }

  ngOnInit(): void {
  }

}
