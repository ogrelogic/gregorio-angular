import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FromCiampinoAirportComponent } from './from-ciampino-airport.component';

describe('FromCiampinoAirportComponent', () => {
  let component: FromCiampinoAirportComponent;
  let fixture: ComponentFixture<FromCiampinoAirportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FromCiampinoAirportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FromCiampinoAirportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
