import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-from-ciampino-airport',
  templateUrl: './from-ciampino-airport.component.html',
  styleUrls: ['./from-ciampino-airport.component.css']
})
export class FromCiampinoAirportComponent implements OnInit {

  
  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.directionpages().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].FROMCIAMPINOAIRPORT;
      console.warn(this.directionpagedata1);
    });
  }


  ngOnInit(): void {
  }

}
