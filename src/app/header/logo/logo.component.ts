import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MyserviceService } from 'src/app/myservice.service'; 

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {


  mydashboard=AppComponent.mydashboard;

  logospart:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.headerfoorer().subscribe(( data) =>{
      this.logospart=data;
      //this.directionpagedata1= this.directionpages[0].FromFiumicinoAirport;
      //console.warn(this.directionpagedata1);
    });
  }

  ngOnInit(): void {
  }

  myimage = AppComponent.myimage;
}
