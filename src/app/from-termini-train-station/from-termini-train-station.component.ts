import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component'; 
import { MyserviceService } from '../myservice.service';  

@Component({
  selector: 'app-from-termini-train-station',
  templateUrl: './from-termini-train-station.component.html',
  styleUrls: ['./from-termini-train-station.component.css']
})
export class FromTerminiTrainStationComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.directionpages().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].FROMTERMINITRAINSTATION;
      console.warn(this.directionpagedata1);
    });
  }


  ngOnInit(): void {
  }

}
