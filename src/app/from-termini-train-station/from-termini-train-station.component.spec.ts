import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FromTerminiTrainStationComponent } from './from-termini-train-station.component';

describe('FromTerminiTrainStationComponent', () => {
  let component: FromTerminiTrainStationComponent;
  let fixture: ComponentFixture<FromTerminiTrainStationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FromTerminiTrainStationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FromTerminiTrainStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
