import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  data='';
  constructor(private routers:ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.routers.snapshot.params);
    this.data = this.routers.snapshot.params['id'];
  }

}
