import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SuitsPageComponent } from './suits-page/suits-page.component';
import { DirectionPagesComponent } from './direction-pages/direction-pages.component';
import { FromCiampinoAirportComponent } from './from-ciampino-airport/from-ciampino-airport.component';
import { FromTerminiTrainStationComponent } from './from-termini-train-station/from-termini-train-station.component';
import { HotelToCityCenterComponent } from './hotel-to-city-center/hotel-to-city-center.component';
import { SpecialOffersComponent } from './special-offers/special-offers.component';
import { ServicespageComponent } from './servicespage/servicespage.component';
import { NearByPageComponent } from './near-by-page/near-by-page.component';
import { GallerypagesComponent } from './gallerypages/gallerypages.component';
import { NewsComponent } from './news/news.component';
import { ImgpartComponent } from './home/component/home4/imgpart/imgpart.component';
import { SinglePostComponent } from './single-post/single-post.component';
import { RomanticHolidayPackageComponent } from './romantic-holiday-package/romantic-holiday-package.component';
import { RomanticWeekendPackagesComponent } from './romantic-weekend-packages/romantic-weekend-packages.component'; 
import { HoneymoonSuiteComponent } from './honeymoon-suite/honeymoon-suite.component';
import { SuperiorSuiteComponent } from './superior-suite/superior-suite.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { BookingComponent } from './booking/booking.component';


const routes: Routes = [
  { path: '',   component: HomeComponent }, 
  { path: 'Suitespage',  component: SuitsPageComponent }, 
  { path: 'from-fiumicino-airport',  component: DirectionPagesComponent },
  { path: 'from-ciampino-airport',  component: FromCiampinoAirportComponent },
  { path: 'from-termini-train-station',  component: FromTerminiTrainStationComponent }, 
  { path: 'hotel-to-city-center',  component: HotelToCityCenterComponent }, 
  { path: 'special-offers',  component: SpecialOffersComponent }, 
  { path: 'services',  component: ServicespageComponent }, 
  { path: 'near-by-page',  component:  NearByPageComponent}, 
  { path: 'gallery',  component:  GallerypagesComponent}, 
  { path: 'Home',  component:  HomeComponent},
  { path: 'movie',  component:  ImgpartComponent},
  { path: 'News',  component:  NewsComponent},
  { path: 'single-post/:id',  component:  SinglePostComponent}, 
  { path: 'romantic-holiday-package',  component:  RomanticHolidayPackageComponent},
  { path: 'romantic-weekend-package',  component:  RomanticWeekendPackagesComponent},
  { path: 'honeymoon-suite',  component:  HoneymoonSuiteComponent},
  { path: 'superior-suite',  component:  SuperiorSuiteComponent},
  { path: 'contact',  component:  ContactFormComponent},
  { path: 'Booking', component:  BookingComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
