import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  suitesdatapage:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.Suitespages().subscribe(( data) =>{
      this.suitesdatapage=data;
      //console.warn(data);
    });

  }
   
  ngOnInit(): void {
  }

}
