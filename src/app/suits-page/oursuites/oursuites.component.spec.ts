import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OursuitesComponent } from './oursuites.component';

describe('OursuitesComponent', () => {
  let component: OursuitesComponent;
  let fixture: ComponentFixture<OursuitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OursuitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OursuitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
