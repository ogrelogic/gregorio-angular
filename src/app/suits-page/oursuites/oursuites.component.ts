import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-oursuites',
  templateUrl: './oursuites.component.html',
  styleUrls: ['./oursuites.component.css']
})
export class OursuitesComponent implements OnInit {

  //dashboardurl=AppComponent.dashboardurl;
  myimage=AppComponent.myimage;
  urldashboard=AppComponent.urldashboard;

  suitesdatapage:any;
  suitesPost:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.Suitespages().subscribe(( data) =>{
      this.suitesdatapage = data;
      this.suitesPost = this.suitesdatapage[0].demos;
      console.warn(this.suitesPost);
    });

  }

  ngOnInit(): void {
  }

}
