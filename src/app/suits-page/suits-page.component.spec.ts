import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitsPageComponent } from './suits-page.component';

describe('SuitsPageComponent', () => {
  let component: SuitsPageComponent;
  let fixture: ComponentFixture<SuitsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuitsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
