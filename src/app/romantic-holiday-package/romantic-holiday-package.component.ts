import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-romantic-holiday-package',
  templateUrl: './romantic-holiday-package.component.html',
  styleUrls: ['./romantic-holiday-package.component.css']
})
export class RomanticHolidayPackageComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.romanticholiday().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].romanticholidaypackage;
      console.warn(this.directionpagedata1);
    });
  }


  ngOnInit(): void {
  }

}
