import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RomanticHolidayPackageComponent } from './romantic-holiday-package.component';

describe('RomanticHolidayPackageComponent', () => {
  let component: RomanticHolidayPackageComponent;
  let fixture: ComponentFixture<RomanticHolidayPackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RomanticHolidayPackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RomanticHolidayPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
