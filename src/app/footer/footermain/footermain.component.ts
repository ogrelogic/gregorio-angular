import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';

@Component({
  selector: 'app-footermain',
  templateUrl: './footermain.component.html',
  styleUrls: ['./footermain.component.css']
})
export class FootermainComponent implements OnInit {
  socialslinkss:any;
  logospart:any;
  constructor(private userData:MyserviceService, private userData1:MyserviceService) { 
    this.userData.social().subscribe(( data) =>{
      this.socialslinkss=data;
      //console.warn(data);
    });
    this.userData1.headerfoorer().subscribe(( data) =>{
      this.logospart=data;
    });
  }

  ngOnInit(): void {
  }

  myimage:string = '../assets/images/';

}
