import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MyserviceService } from 'src/app/myservice.service';

@Component({
  selector: 'app-servicescomponent',
  templateUrl: './servicescomponent.component.html',
  styleUrls: ['./servicescomponent.component.css']
})
export class ServicescomponentComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  servicepage:any;
  servicepagedata1:any;
  servicepagecobbbmponent:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.services().subscribe(( data) =>{
      this.servicepage=data;
      this.servicepagedata1= this.servicepage[0].services;
      this.servicepagecobbbmponent= this.servicepage[0].servicecomponent;
      console.table(this.servicepagecobbbmponent);
    });
  }

  ngOnInit(): void {
  }

}
