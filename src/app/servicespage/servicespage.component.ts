import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-servicespage',
  templateUrl: './servicespage.component.html',
  styleUrls: ['./servicespage.component.css']
})
export class ServicespageComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  servicepage:any;
  servicepagedata1:any;
  servicepagecobbbmponent:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.services().subscribe(( data) =>{
      this.servicepage=data;
      this.servicepagedata1= this.servicepage[0].services;
      this.servicepagecobbbmponent= this.servicepage[0].servicecomponent;
      console.table(this.servicepagecobbbmponent);
    });
    
  }

  ngOnInit(): void {
  }

}
