import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;
  blogpages:any;
  blogpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.blogpages().subscribe(( data) =>{
      this.blogpages=data;
      this.blogpagedata1= this.blogpages[0].Blogpage;
      console.warn(this.blogpagedata1);
    });
  }

  ngOnInit(): void {
  }

}
