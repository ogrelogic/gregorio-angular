import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-near-by-page',
  templateUrl: './near-by-page.component.html',
  styleUrls: ['./near-by-page.component.css']
})
export class NearByPageComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.nearbypage().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].nearbypost;
      console.warn(this.directionpagedata1);
    });
  }


  ngOnInit(): void {
  }

}
