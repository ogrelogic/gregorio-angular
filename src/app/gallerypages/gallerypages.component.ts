import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-gallerypages',
  templateUrl: './gallerypages.component.html',
  styleUrls: ['./gallerypages.component.css']
})
export class GallerypagesComponent implements OnInit {

  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.gallerypage().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].Gallerypage.image;
      //console.log(this.directionpagedata1);
    });
  }

  ngOnInit(): void {
  }

}
