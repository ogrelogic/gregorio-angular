import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';

@Component({
  selector: 'app-social-links-url',
  templateUrl: './social-links-url.component.html',
  styleUrls: ['./social-links-url.component.css']
})
export class SocialLinksUrlComponent implements OnInit {

  socialslinkss:any;
  constructor(private userData:MyserviceService) { 
    this.userData.social().subscribe(( data) =>{
      this.socialslinkss=data;
      //console.warn(data);
    });
  }

  ngOnInit(): void {
  }

}
