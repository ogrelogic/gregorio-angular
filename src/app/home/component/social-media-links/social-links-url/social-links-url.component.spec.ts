import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialLinksUrlComponent } from './social-links-url.component';

describe('SocialLinksUrlComponent', () => {
  let component: SocialLinksUrlComponent;
  let fixture: ComponentFixture<SocialLinksUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialLinksUrlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialLinksUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
