import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MyserviceService } from 'src/app/myservice.service';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home3apiurl',
  templateUrl: './home3apiurl.component.html',
  styleUrls: ['./home3apiurl.component.css']
})
export class Home3apiurlComponent implements OnInit {

  dashboardurl = AppComponent.mydashboard;
  myimage = AppComponent.myimage;

  homegallery:any;
  fuldata:any;
  fuldata2:any;
  fuldata3:any;
  thirdata:any;
  constructor(private userData:MyserviceService, ) { 
    this.userData.user().subscribe(( data) =>{
      this.thirdata=data;
      //console.warn(data);
    });
   

    this.userData.homegallery().subscribe(( data) =>{
      this.homegallery=data;
      this.fuldata=this.homegallery[0].images;
      this.fuldata2=this.homegallery[1].images;
      this.fuldata3=this.homegallery[2].images;
      console.warn(this.fuldata.url);
    });

  }
  ngOnInit(): void {
  }

   

  customOptions2: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    autoplay:true,
    dots: false,
    items: 4,
    margin:20,

    navSpeed: 900,
    navText: ['>', '<'],
    responsive: {
      0: {
        items: 3
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    nav: false
  }

  customOptions3: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    autoplay:true,
    dots: false,
    items: 4,
    margin:20,

    navSpeed: 900,
    navText: ['>', '<'],
    responsive: {
      0: {
        items: 4
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 4
      }
    },
    nav: false
  }

}

