import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Home3apiurlComponent } from './home3apiurl.component';

describe('Home3apiurlComponent', () => {
  let component: Home3apiurlComponent;
  let fixture: ComponentFixture<Home3apiurlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Home3apiurlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Home3apiurlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
