import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgpartComponent } from './imgpart.component';

describe('ImgpartComponent', () => {
  let component: ImgpartComponent;
  let fixture: ComponentFixture<ImgpartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImgpartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgpartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
