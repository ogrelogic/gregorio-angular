import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imgpart',
  templateUrl: './imgpart.component.html',
  styleUrls: ['./imgpart.component.css']
})
export class ImgpartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  myimage:string = '../assets/images/';
  myanothercsslink:string = 'src/home/component/home4/imgpart/imgpart.component.css';

}
