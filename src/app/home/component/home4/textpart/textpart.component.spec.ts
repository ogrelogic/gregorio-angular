import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextpartComponent } from './textpart.component';

describe('TextpartComponent', () => {
  let component: TextpartComponent;
  let fixture: ComponentFixture<TextpartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextpartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextpartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
