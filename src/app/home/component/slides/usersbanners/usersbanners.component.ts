import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
/*import { HttpClient } from '@angular/common/http'*/
//import { SlidesComponent } from "../slides.component";
import { MyserviceService } from 'src/app/myservice.service';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Injectable({
    providedIn: 'root'
})

@Component({
  selector: 'app-usersbanners',
  templateUrl: './usersbanners.component.html',
  styleUrls: ['./usersbanners.component.css']
})
export class UsersbannersComponent implements OnInit {
  element:any;
  users:any;
  constructor(private userData:MyserviceService) 
  { 
    this.userData.users().subscribe((data) =>{
      this.users=data;
      
    });
  }
  myimage:string = '../assets/images/';

  mydashboard:string = 'http://50.18.189.215:1337';
  
  ngOnInit(): void {
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    autoplay:true,
    dots: true,
    items: 1,
    navSpeed: 5000,
    navText: ['>', '<'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 1
      }
    },
    nav: false
  }
  

}
