import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersbannersComponent } from './usersbanners.component';

describe('UsersbannersComponent', () => {
  let component: UsersbannersComponent;
  let fixture: ComponentFixture<UsersbannersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersbannersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersbannersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
