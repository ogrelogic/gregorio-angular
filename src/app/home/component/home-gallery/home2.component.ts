import { Component, OnInit } from '@angular/core';
import { MyserviceService } from 'src/app/myservice.service';
import { AppComponent } from 'src/app/app.component';


@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.css']
})
export class Home2Component implements OnInit {

  dashboardurl = AppComponent.mydashboard;
  myimage = AppComponent.myimage;

  homegallery:any;
  constructor(private userData:MyserviceService) { 
    this.userData.homegallery().subscribe(( data) =>{
      this.homegallery=data;
      //console.warn(data);
    });
  }

  ngOnInit(): void {
  }

}
