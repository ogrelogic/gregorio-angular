import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RomanticWeekendPackagesComponent } from './romantic-weekend-packages.component';

describe('RomanticWeekendPackagesComponent', () => {
  let component: RomanticWeekendPackagesComponent;
  let fixture: ComponentFixture<RomanticWeekendPackagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RomanticWeekendPackagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RomanticWeekendPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
