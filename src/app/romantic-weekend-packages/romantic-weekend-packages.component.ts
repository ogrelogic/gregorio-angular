import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-romantic-weekend-packages',
  templateUrl: './romantic-weekend-packages.component.html',
  styleUrls: ['./romantic-weekend-packages.component.css']
})
export class RomanticWeekendPackagesComponent implements OnInit {

  
  mydashboard=AppComponent.mydashboard;

  directionpages:any;
  directionpagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.romanticweekend().subscribe(( data) =>{
      this.directionpages=data;
      this.directionpagedata1= this.directionpages[0].romanticweekendpackages;
      console.warn(this.directionpagedata1);
    });
  }


  ngOnInit(): void {
  }

}
