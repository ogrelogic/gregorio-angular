import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-special-offers',
  templateUrl: './special-offers.component.html',
  styleUrls: ['./special-offers.component.css']
})
export class SpecialOffersComponent implements OnInit {

  
  mydashboard=AppComponent.mydashboard;
  myimage=AppComponent.myimage;

  specialofferspages:any;
  specialofferspagedata1:any;
  constructor(private userData1:MyserviceService) { 
    this.userData1.specialoffers().subscribe(( data) =>{
      this.specialofferspages=data;
      this.specialofferspagedata1= this.specialofferspages[0].specialoffersds;
      console.warn(this.specialofferspagedata1);
    });
  }

  ngOnInit(): void {
  }

}
